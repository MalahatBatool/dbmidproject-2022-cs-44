﻿namespace ProjectB
{
    partial class studentsmanage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnpanel = new System.Windows.Forms.Panel();
            this.guna2GradientPanel1 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.guna2GradientButton4 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2GradientButton3 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2GradientButton2 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2GradientButton1 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.label1 = new System.Windows.Forms.Label();
            this.addpanel = new System.Windows.Forms.Panel();
            this.updatepanel = new System.Windows.Forms.Panel();
            this.deletepanel = new System.Windows.Forms.Panel();
            this.guna2GradientButton9 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.guna2GradientButton10 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.label11 = new System.Windows.Forms.Label();
            this.guna2GradientButton8 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.guna2GradientButton7 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.guna2GradientButton6 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2GradientButton5 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.viewStudent = new System.Windows.Forms.Panel();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.guna2GradientButton12 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.label12 = new System.Windows.Forms.Label();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.update = new System.Windows.Forms.Label();
            this.btnpanel.SuspendLayout();
            this.guna2GradientPanel1.SuspendLayout();
            this.addpanel.SuspendLayout();
            this.updatepanel.SuspendLayout();
            this.deletepanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.viewStudent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // btnpanel
            // 
            this.btnpanel.Controls.Add(this.addpanel);
            this.btnpanel.Controls.Add(this.guna2GradientPanel1);
            this.btnpanel.Controls.Add(this.guna2GradientButton4);
            this.btnpanel.Controls.Add(this.guna2GradientButton3);
            this.btnpanel.Controls.Add(this.guna2GradientButton2);
            this.btnpanel.Controls.Add(this.guna2GradientButton1);
            this.btnpanel.Controls.Add(this.label1);
            this.btnpanel.Location = new System.Drawing.Point(3, 3);
            this.btnpanel.Name = "btnpanel";
            this.btnpanel.Size = new System.Drawing.Size(574, 408);
            this.btnpanel.TabIndex = 0;
            // 
            // guna2GradientPanel1
            // 
            this.guna2GradientPanel1.BorderRadius = 10;
            this.guna2GradientPanel1.Controls.Add(this.label2);
            this.guna2GradientPanel1.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientPanel1.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientPanel1.Location = new System.Drawing.Point(176, 103);
            this.guna2GradientPanel1.Name = "guna2GradientPanel1";
            this.guna2GradientPanel1.Size = new System.Drawing.Size(230, 44);
            this.guna2GradientPanel1.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Franklin Gothic Demi Cond", 14.25F);
            this.label2.ForeColor = System.Drawing.Color.Crimson;
            this.label2.Location = new System.Drawing.Point(4, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(221, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "Choose one of the Following:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // guna2GradientButton4
            // 
            this.guna2GradientButton4.BorderRadius = 10;
            this.guna2GradientButton4.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton4.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton4.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton4.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton4.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton4.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton4.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton4.Location = new System.Drawing.Point(242, 332);
            this.guna2GradientButton4.Name = "guna2GradientButton4";
            this.guna2GradientButton4.Size = new System.Drawing.Size(97, 29);
            this.guna2GradientButton4.TabIndex = 5;
            this.guna2GradientButton4.Text = "View";
            this.guna2GradientButton4.Click += new System.EventHandler(this.guna2GradientButton4_Click);
            // 
            // guna2GradientButton3
            // 
            this.guna2GradientButton3.BorderRadius = 10;
            this.guna2GradientButton3.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton3.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton3.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton3.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton3.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton3.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton3.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton3.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton3.Location = new System.Drawing.Point(242, 278);
            this.guna2GradientButton3.Name = "guna2GradientButton3";
            this.guna2GradientButton3.Size = new System.Drawing.Size(97, 29);
            this.guna2GradientButton3.TabIndex = 4;
            this.guna2GradientButton3.Text = "Delete";
            this.guna2GradientButton3.Click += new System.EventHandler(this.guna2GradientButton3_Click);
            // 
            // guna2GradientButton2
            // 
            this.guna2GradientButton2.BorderRadius = 10;
            this.guna2GradientButton2.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton2.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton2.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton2.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton2.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton2.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton2.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton2.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton2.Location = new System.Drawing.Point(242, 227);
            this.guna2GradientButton2.Name = "guna2GradientButton2";
            this.guna2GradientButton2.Size = new System.Drawing.Size(97, 29);
            this.guna2GradientButton2.TabIndex = 3;
            this.guna2GradientButton2.Text = "Update";
            this.guna2GradientButton2.Click += new System.EventHandler(this.guna2GradientButton2_Click);
            // 
            // guna2GradientButton1
            // 
            this.guna2GradientButton1.BorderRadius = 10;
            this.guna2GradientButton1.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton1.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton1.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton1.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton1.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton1.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton1.Location = new System.Drawing.Point(242, 174);
            this.guna2GradientButton1.Name = "guna2GradientButton1";
            this.guna2GradientButton1.Size = new System.Drawing.Size(97, 29);
            this.guna2GradientButton1.TabIndex = 2;
            this.guna2GradientButton1.Text = "Add";
            this.guna2GradientButton1.Click += new System.EventHandler(this.guna2GradientButton1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Demi", 15.75F);
            this.label1.ForeColor = System.Drawing.Color.Crimson;
            this.label1.Location = new System.Drawing.Point(206, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Manage Students";
            // 
            // addpanel
            // 
            this.addpanel.Controls.Add(this.updatepanel);
            this.addpanel.Controls.Add(this.guna2GradientButton6);
            this.addpanel.Controls.Add(this.guna2GradientButton5);
            this.addpanel.Controls.Add(this.textBox7);
            this.addpanel.Controls.Add(this.textBox6);
            this.addpanel.Controls.Add(this.textBox5);
            this.addpanel.Controls.Add(this.textBox4);
            this.addpanel.Controls.Add(this.textBox3);
            this.addpanel.Controls.Add(this.textBox2);
            this.addpanel.Controls.Add(this.textBox1);
            this.addpanel.Controls.Add(this.label10);
            this.addpanel.Controls.Add(this.label9);
            this.addpanel.Controls.Add(this.label8);
            this.addpanel.Controls.Add(this.label7);
            this.addpanel.Controls.Add(this.label6);
            this.addpanel.Controls.Add(this.label5);
            this.addpanel.Controls.Add(this.label4);
            this.addpanel.Controls.Add(this.label3);
            this.addpanel.Location = new System.Drawing.Point(0, 0);
            this.addpanel.Name = "addpanel";
            this.addpanel.Size = new System.Drawing.Size(574, 408);
            this.addpanel.TabIndex = 7;
            this.addpanel.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // updatepanel
            // 
            this.updatepanel.Controls.Add(this.deletepanel);
            this.updatepanel.Controls.Add(this.guna2GradientButton8);
            this.updatepanel.Controls.Add(this.dataGridView1);
            this.updatepanel.Controls.Add(this.guna2GradientButton7);
            this.updatepanel.Controls.Add(this.comboBox1);
            this.updatepanel.Controls.Add(this.update);
            this.updatepanel.Location = new System.Drawing.Point(0, 0);
            this.updatepanel.Name = "updatepanel";
            this.updatepanel.Size = new System.Drawing.Size(574, 408);
            this.updatepanel.TabIndex = 17;
            // 
            // deletepanel
            // 
            this.deletepanel.Controls.Add(this.guna2GradientButton9);
            this.deletepanel.Controls.Add(this.dataGridView2);
            this.deletepanel.Controls.Add(this.guna2GradientButton10);
            this.deletepanel.Controls.Add(this.label11);
            this.deletepanel.Location = new System.Drawing.Point(0, 0);
            this.deletepanel.Name = "deletepanel";
            this.deletepanel.Size = new System.Drawing.Size(574, 408);
            this.deletepanel.TabIndex = 20;
            // 
            // guna2GradientButton9
            // 
            this.guna2GradientButton9.BorderRadius = 10;
            this.guna2GradientButton9.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton9.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton9.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton9.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton9.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton9.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton9.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton9.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton9.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton9.Location = new System.Drawing.Point(367, 331);
            this.guna2GradientButton9.Name = "guna2GradientButton9";
            this.guna2GradientButton9.Size = new System.Drawing.Size(112, 32);
            this.guna2GradientButton9.TabIndex = 23;
            this.guna2GradientButton9.Text = "Delete";
            this.guna2GradientButton9.Click += new System.EventHandler(this.guna2GradientButton9_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(44, 109);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(487, 194);
            this.dataGridView2.TabIndex = 22;
            // 
            // guna2GradientButton10
            // 
            this.guna2GradientButton10.BorderRadius = 10;
            this.guna2GradientButton10.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton10.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton10.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton10.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton10.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton10.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton10.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton10.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton10.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton10.Location = new System.Drawing.Point(102, 331);
            this.guna2GradientButton10.Name = "guna2GradientButton10";
            this.guna2GradientButton10.Size = new System.Drawing.Size(112, 32);
            this.guna2GradientButton10.TabIndex = 21;
            this.guna2GradientButton10.Text = "Back";
            this.guna2GradientButton10.Click += new System.EventHandler(this.guna2GradientButton10_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Franklin Gothic Demi", 14.25F);
            this.label11.ForeColor = System.Drawing.Color.Crimson;
            this.label11.Location = new System.Drawing.Point(225, 46);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(134, 24);
            this.label11.TabIndex = 20;
            this.label11.Text = "Delete Student";
            // 
            // guna2GradientButton8
            // 
            this.guna2GradientButton8.BorderRadius = 10;
            this.guna2GradientButton8.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton8.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton8.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton8.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton8.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton8.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton8.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton8.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton8.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton8.Location = new System.Drawing.Point(365, 321);
            this.guna2GradientButton8.Name = "guna2GradientButton8";
            this.guna2GradientButton8.Size = new System.Drawing.Size(112, 32);
            this.guna2GradientButton8.TabIndex = 19;
            this.guna2GradientButton8.Text = "Update";
            this.guna2GradientButton8.Click += new System.EventHandler(this.guna2GradientButton8_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(42, 99);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(487, 194);
            this.dataGridView1.TabIndex = 18;
            // 
            // guna2GradientButton7
            // 
            this.guna2GradientButton7.BorderRadius = 10;
            this.guna2GradientButton7.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton7.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton7.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton7.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton7.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton7.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton7.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton7.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton7.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton7.Location = new System.Drawing.Point(100, 321);
            this.guna2GradientButton7.Name = "guna2GradientButton7";
            this.guna2GradientButton7.Size = new System.Drawing.Size(112, 32);
            this.guna2GradientButton7.TabIndex = 17;
            this.guna2GradientButton7.Text = "Back";
            this.guna2GradientButton7.Click += new System.EventHandler(this.guna2GradientButton7_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.LavenderBlush;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Id",
            "FirstName",
            "LastName",
            "Contact",
            "Email",
            "Registration Number",
            "Status",
            ""});
            this.comboBox1.Location = new System.Drawing.Point(408, 63);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 23;
            // 
            // guna2GradientButton6
            // 
            this.guna2GradientButton6.BorderRadius = 10;
            this.guna2GradientButton6.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton6.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton6.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton6.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton6.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton6.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton6.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton6.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton6.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton6.Location = new System.Drawing.Point(84, 348);
            this.guna2GradientButton6.Name = "guna2GradientButton6";
            this.guna2GradientButton6.Size = new System.Drawing.Size(112, 32);
            this.guna2GradientButton6.TabIndex = 16;
            this.guna2GradientButton6.Text = "Back";
            this.guna2GradientButton6.Click += new System.EventHandler(this.guna2GradientButton6_Click);
            // 
            // guna2GradientButton5
            // 
            this.guna2GradientButton5.BorderRadius = 10;
            this.guna2GradientButton5.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton5.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton5.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton5.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton5.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton5.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton5.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton5.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton5.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton5.Location = new System.Drawing.Point(365, 348);
            this.guna2GradientButton5.Name = "guna2GradientButton5";
            this.guna2GradientButton5.Size = new System.Drawing.Size(112, 32);
            this.guna2GradientButton5.TabIndex = 15;
            this.guna2GradientButton5.Text = "Add";
            this.guna2GradientButton5.Click += new System.EventHandler(this.guna2GradientButton5_Click);
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(277, 299);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(200, 20);
            this.textBox7.TabIndex = 14;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(277, 264);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(200, 20);
            this.textBox6.TabIndex = 13;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(277, 229);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(200, 20);
            this.textBox5.TabIndex = 12;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(277, 197);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(200, 20);
            this.textBox4.TabIndex = 11;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(277, 161);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(200, 20);
            this.textBox3.TabIndex = 10;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(277, 124);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(200, 20);
            this.textBox2.TabIndex = 9;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(277, 90);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(200, 20);
            this.textBox1.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Italic);
            this.label10.ForeColor = System.Drawing.Color.Crimson;
            this.label10.Location = new System.Drawing.Point(67, 297);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 21);
            this.label10.TabIndex = 7;
            this.label10.Text = "Enter Status:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Italic);
            this.label9.ForeColor = System.Drawing.Color.Crimson;
            this.label9.Location = new System.Drawing.Point(67, 262);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(195, 21);
            this.label9.TabIndex = 6;
            this.label9.Text = "Enter Registration Number:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Italic);
            this.label8.ForeColor = System.Drawing.Color.Crimson;
            this.label8.Location = new System.Drawing.Point(67, 228);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 21);
            this.label8.TabIndex = 5;
            this.label8.Text = "Enter Email:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Italic);
            this.label7.ForeColor = System.Drawing.Color.Crimson;
            this.label7.Location = new System.Drawing.Point(67, 195);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 21);
            this.label7.TabIndex = 4;
            this.label7.Text = "Enter Contact Number:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Italic);
            this.label6.ForeColor = System.Drawing.Color.Crimson;
            this.label6.Location = new System.Drawing.Point(67, 161);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 21);
            this.label6.TabIndex = 3;
            this.label6.Text = "Enter Last Name:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Italic);
            this.label5.ForeColor = System.Drawing.Color.Crimson;
            this.label5.Location = new System.Drawing.Point(67, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 21);
            this.label5.TabIndex = 2;
            this.label5.Text = "Enter First Name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Italic);
            this.label4.ForeColor = System.Drawing.Color.Crimson;
            this.label4.Location = new System.Drawing.Point(67, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 21);
            this.label4.TabIndex = 1;
            this.label4.Text = "Enter Student ID:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Franklin Gothic Demi", 14.25F);
            this.label3.ForeColor = System.Drawing.Color.Crimson;
            this.label3.Location = new System.Drawing.Point(229, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "Add Student";
            // 
            // viewStudent
            // 
            this.viewStudent.Controls.Add(this.dataGridView3);
            this.viewStudent.Controls.Add(this.guna2GradientButton12);
            this.viewStudent.Controls.Add(this.label12);
            this.viewStudent.Location = new System.Drawing.Point(3, 0);
            this.viewStudent.Name = "viewStudent";
            this.viewStudent.Size = new System.Drawing.Size(574, 408);
            this.viewStudent.TabIndex = 24;
            this.viewStudent.Paint += new System.Windows.Forms.PaintEventHandler(this.viewStudent_Paint);
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(44, 109);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(487, 194);
            this.dataGridView3.TabIndex = 26;
            // 
            // guna2GradientButton12
            // 
            this.guna2GradientButton12.BorderRadius = 10;
            this.guna2GradientButton12.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton12.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton12.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton12.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton12.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton12.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton12.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton12.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton12.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton12.Location = new System.Drawing.Point(227, 332);
            this.guna2GradientButton12.Name = "guna2GradientButton12";
            this.guna2GradientButton12.Size = new System.Drawing.Size(112, 32);
            this.guna2GradientButton12.TabIndex = 25;
            this.guna2GradientButton12.Text = "Back";
            this.guna2GradientButton12.Click += new System.EventHandler(this.guna2GradientButton12_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Franklin Gothic Demi", 14.25F);
            this.label12.ForeColor = System.Drawing.Color.Crimson;
            this.label12.Location = new System.Drawing.Point(225, 46);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(120, 24);
            this.label12.TabIndex = 24;
            this.label12.Text = "View Student";
            // 
            // update
            // 
            this.update.AutoSize = true;
            this.update.Font = new System.Drawing.Font("Franklin Gothic Demi", 14.25F);
            this.update.ForeColor = System.Drawing.Color.Crimson;
            this.update.Location = new System.Drawing.Point(221, 36);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(139, 24);
            this.update.TabIndex = 25;
            this.update.Text = "Update Student";
            // 
            // studentsmanage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LavenderBlush;
            this.Controls.Add(this.viewStudent);
            this.Controls.Add(this.btnpanel);
            this.Name = "studentsmanage";
            this.Size = new System.Drawing.Size(580, 414);
            this.btnpanel.ResumeLayout(false);
            this.btnpanel.PerformLayout();
            this.guna2GradientPanel1.ResumeLayout(false);
            this.guna2GradientPanel1.PerformLayout();
            this.addpanel.ResumeLayout(false);
            this.addpanel.PerformLayout();
            this.updatepanel.ResumeLayout(false);
            this.updatepanel.PerformLayout();
            this.deletepanel.ResumeLayout(false);
            this.deletepanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.viewStudent.ResumeLayout(false);
            this.viewStudent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel btnpanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton4;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton3;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton2;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton1;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel1;
        private System.Windows.Forms.Panel addpanel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton6;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton5;
        private System.Windows.Forms.Panel updatepanel;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton7;
        private System.Windows.Forms.Panel deletepanel;
        private System.Windows.Forms.Panel viewStudent;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton9;
        private System.Windows.Forms.DataGridView dataGridView2;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton10;
        private System.Windows.Forms.Label label11;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton8;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.DataGridView dataGridView3;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton12;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label update;
    }
}
