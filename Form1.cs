﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            customizeDesign();

            home11.Visible= false;
            studentsmanage1.Visible=false;
            manage_CLOs1.Visible=false;
            manageRubric1.Visible=false;
        }
        private void customizeDesign()
        {
            SubMenu.Visible = false;
        }
        private void hideSubMenu()
        {
            if (SubMenu.Visible == true)
                SubMenu.Visible = false;

        }
        private void showSubMenu(Panel SubMenu)
        {
            if (SubMenu.Visible == false)
            {
                hideSubMenu();
                SubMenu.Visible = true;
            }
            else
                SubMenu.Visible = false;
        }
        private void guna2Button1_Click(object sender, EventArgs e)
        {
            showSubMenu(SubMenu);
        }

        private void studentsmanage1_Load(object sender, EventArgs e)
        {

        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            //Manage Students(menu)
            studentsmanage1.Visible = true;
            home11.Visible = false;
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            //home(menu)
            home11.Visible = true;
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            //Manage CLOs(menu)
            manage_CLOs1.Visible = true;
            studentsmanage1.Visible = false;
            home11.Visible = false;
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            //Manage rubrics(menu)
            manageRubric1.Visible = true;
            manage_CLOs1.Visible = false;
            studentsmanage1.Visible = false;
            home11.Visible = false;
        }
    }
}
