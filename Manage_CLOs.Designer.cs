﻿namespace ProjectB
{
    partial class Manage_CLOs
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CLOs_btn_panel = new System.Windows.Forms.Panel();
            this.addpanel_clos = new System.Windows.Forms.Panel();
            this.guna2DateTimePicker2 = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.guna2DateTimePicker1 = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.guna2GradientButton6 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2GradientButton5 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.guna2GradientPanel1 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.guna2GradientButton4 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2GradientButton3 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2GradientButton2 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2GradientButton1 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.label1 = new System.Windows.Forms.Label();
            this.updatepanel_clo = new System.Windows.Forms.Panel();
            this.deletepanel_clo = new System.Windows.Forms.Panel();
            this.guna2GradientButton9 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.guna2GradientButton10 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.label11 = new System.Windows.Forms.Label();
            this.guna2GradientButton8 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.guna2GradientButton7 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.update = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.viewStudent_clo = new System.Windows.Forms.Panel();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.guna2GradientButton12 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.label12 = new System.Windows.Forms.Label();
            this.CLOs_btn_panel.SuspendLayout();
            this.addpanel_clos.SuspendLayout();
            this.guna2GradientPanel1.SuspendLayout();
            this.updatepanel_clo.SuspendLayout();
            this.deletepanel_clo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.viewStudent_clo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // CLOs_btn_panel
            // 
            this.CLOs_btn_panel.Controls.Add(this.addpanel_clos);
            this.CLOs_btn_panel.Controls.Add(this.guna2GradientPanel1);
            this.CLOs_btn_panel.Controls.Add(this.guna2GradientButton4);
            this.CLOs_btn_panel.Controls.Add(this.guna2GradientButton3);
            this.CLOs_btn_panel.Controls.Add(this.guna2GradientButton2);
            this.CLOs_btn_panel.Controls.Add(this.guna2GradientButton1);
            this.CLOs_btn_panel.Controls.Add(this.label1);
            this.CLOs_btn_panel.Location = new System.Drawing.Point(3, 3);
            this.CLOs_btn_panel.Name = "CLOs_btn_panel";
            this.CLOs_btn_panel.Size = new System.Drawing.Size(574, 408);
            this.CLOs_btn_panel.TabIndex = 0;
            // 
            // addpanel_clos
            // 
            this.addpanel_clos.Controls.Add(this.updatepanel_clo);
            this.addpanel_clos.Controls.Add(this.guna2DateTimePicker2);
            this.addpanel_clos.Controls.Add(this.guna2DateTimePicker1);
            this.addpanel_clos.Controls.Add(this.guna2GradientButton6);
            this.addpanel_clos.Controls.Add(this.guna2GradientButton5);
            this.addpanel_clos.Controls.Add(this.textBox2);
            this.addpanel_clos.Controls.Add(this.label7);
            this.addpanel_clos.Controls.Add(this.label6);
            this.addpanel_clos.Controls.Add(this.label5);
            this.addpanel_clos.Controls.Add(this.label3);
            this.addpanel_clos.Location = new System.Drawing.Point(0, 0);
            this.addpanel_clos.Name = "addpanel_clos";
            this.addpanel_clos.Size = new System.Drawing.Size(574, 408);
            this.addpanel_clos.TabIndex = 13;
            // 
            // guna2DateTimePicker2
            // 
            this.guna2DateTimePicker2.Checked = true;
            this.guna2DateTimePicker2.FillColor = System.Drawing.Color.LightPink;
            this.guna2DateTimePicker2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.guna2DateTimePicker2.Location = new System.Drawing.Point(299, 197);
            this.guna2DateTimePicker2.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.guna2DateTimePicker2.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.guna2DateTimePicker2.Name = "guna2DateTimePicker2";
            this.guna2DateTimePicker2.Size = new System.Drawing.Size(200, 24);
            this.guna2DateTimePicker2.TabIndex = 24;
            this.guna2DateTimePicker2.Value = new System.DateTime(2024, 3, 8, 2, 36, 6, 275);
            // 
            // guna2DateTimePicker1
            // 
            this.guna2DateTimePicker1.Checked = true;
            this.guna2DateTimePicker1.FillColor = System.Drawing.Color.LightPink;
            this.guna2DateTimePicker1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.guna2DateTimePicker1.Location = new System.Drawing.Point(299, 159);
            this.guna2DateTimePicker1.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.guna2DateTimePicker1.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.guna2DateTimePicker1.Name = "guna2DateTimePicker1";
            this.guna2DateTimePicker1.Size = new System.Drawing.Size(200, 24);
            this.guna2DateTimePicker1.TabIndex = 23;
            this.guna2DateTimePicker1.Value = new System.DateTime(2024, 3, 8, 2, 36, 6, 275);
            // 
            // guna2GradientButton6
            // 
            this.guna2GradientButton6.BorderRadius = 10;
            this.guna2GradientButton6.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton6.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton6.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton6.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton6.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton6.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton6.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton6.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton6.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton6.Location = new System.Drawing.Point(93, 274);
            this.guna2GradientButton6.Name = "guna2GradientButton6";
            this.guna2GradientButton6.Size = new System.Drawing.Size(112, 32);
            this.guna2GradientButton6.TabIndex = 22;
            this.guna2GradientButton6.Text = "Back";
            this.guna2GradientButton6.Click += new System.EventHandler(this.guna2GradientButton6_Click);
            // 
            // guna2GradientButton5
            // 
            this.guna2GradientButton5.BorderRadius = 10;
            this.guna2GradientButton5.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton5.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton5.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton5.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton5.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton5.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton5.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton5.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton5.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton5.Location = new System.Drawing.Point(375, 274);
            this.guna2GradientButton5.Name = "guna2GradientButton5";
            this.guna2GradientButton5.Size = new System.Drawing.Size(112, 32);
            this.guna2GradientButton5.TabIndex = 21;
            this.guna2GradientButton5.Text = "Add";
            this.guna2GradientButton5.Click += new System.EventHandler(this.guna2GradientButton5_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(299, 125);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(200, 20);
            this.textBox2.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Italic);
            this.label7.ForeColor = System.Drawing.Color.Crimson;
            this.label7.Location = new System.Drawing.Point(89, 200);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 21);
            this.label7.TabIndex = 16;
            this.label7.Text = "Date Updated:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Italic);
            this.label6.ForeColor = System.Drawing.Color.Crimson;
            this.label6.Location = new System.Drawing.Point(89, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 21);
            this.label6.TabIndex = 15;
            this.label6.Text = "Date Created:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Italic);
            this.label5.ForeColor = System.Drawing.Color.Crimson;
            this.label5.Location = new System.Drawing.Point(89, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 21);
            this.label5.TabIndex = 14;
            this.label5.Text = "Enter Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Franklin Gothic Demi", 14.25F);
            this.label3.ForeColor = System.Drawing.Color.Crimson;
            this.label3.Location = new System.Drawing.Point(247, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 24);
            this.label3.TabIndex = 12;
            this.label3.Text = "Add CLOs";
            // 
            // guna2GradientPanel1
            // 
            this.guna2GradientPanel1.BorderRadius = 10;
            this.guna2GradientPanel1.Controls.Add(this.label2);
            this.guna2GradientPanel1.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientPanel1.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientPanel1.Location = new System.Drawing.Point(172, 102);
            this.guna2GradientPanel1.Name = "guna2GradientPanel1";
            this.guna2GradientPanel1.Size = new System.Drawing.Size(230, 44);
            this.guna2GradientPanel1.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Franklin Gothic Demi Cond", 14.25F);
            this.label2.ForeColor = System.Drawing.Color.Crimson;
            this.label2.Location = new System.Drawing.Point(4, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(221, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "Choose one of the Following:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // guna2GradientButton4
            // 
            this.guna2GradientButton4.BorderRadius = 10;
            this.guna2GradientButton4.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton4.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton4.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton4.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton4.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton4.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton4.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton4.Location = new System.Drawing.Point(238, 331);
            this.guna2GradientButton4.Name = "guna2GradientButton4";
            this.guna2GradientButton4.Size = new System.Drawing.Size(97, 29);
            this.guna2GradientButton4.TabIndex = 11;
            this.guna2GradientButton4.Text = "View";
            this.guna2GradientButton4.Click += new System.EventHandler(this.guna2GradientButton4_Click);
            // 
            // guna2GradientButton3
            // 
            this.guna2GradientButton3.BorderRadius = 10;
            this.guna2GradientButton3.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton3.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton3.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton3.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton3.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton3.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton3.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton3.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton3.Location = new System.Drawing.Point(238, 277);
            this.guna2GradientButton3.Name = "guna2GradientButton3";
            this.guna2GradientButton3.Size = new System.Drawing.Size(97, 29);
            this.guna2GradientButton3.TabIndex = 10;
            this.guna2GradientButton3.Text = "Delete";
            this.guna2GradientButton3.Click += new System.EventHandler(this.guna2GradientButton3_Click);
            // 
            // guna2GradientButton2
            // 
            this.guna2GradientButton2.BorderRadius = 10;
            this.guna2GradientButton2.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton2.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton2.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton2.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton2.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton2.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton2.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton2.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton2.Location = new System.Drawing.Point(238, 226);
            this.guna2GradientButton2.Name = "guna2GradientButton2";
            this.guna2GradientButton2.Size = new System.Drawing.Size(97, 29);
            this.guna2GradientButton2.TabIndex = 9;
            this.guna2GradientButton2.Text = "Update";
            this.guna2GradientButton2.Click += new System.EventHandler(this.guna2GradientButton2_Click);
            // 
            // guna2GradientButton1
            // 
            this.guna2GradientButton1.BorderRadius = 10;
            this.guna2GradientButton1.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton1.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton1.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton1.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton1.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton1.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton1.Location = new System.Drawing.Point(238, 173);
            this.guna2GradientButton1.Name = "guna2GradientButton1";
            this.guna2GradientButton1.Size = new System.Drawing.Size(97, 29);
            this.guna2GradientButton1.TabIndex = 8;
            this.guna2GradientButton1.Text = "Add";
            this.guna2GradientButton1.Click += new System.EventHandler(this.guna2GradientButton1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Demi", 15.75F);
            this.label1.ForeColor = System.Drawing.Color.Crimson;
            this.label1.Location = new System.Drawing.Point(216, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 26);
            this.label1.TabIndex = 7;
            this.label1.Text = "Manage CLOs";
            // 
            // updatepanel_clo
            // 
            this.updatepanel_clo.Controls.Add(this.guna2GradientButton8);
            this.updatepanel_clo.Controls.Add(this.dataGridView1);
            this.updatepanel_clo.Controls.Add(this.guna2GradientButton7);
            this.updatepanel_clo.Controls.Add(this.update);
            this.updatepanel_clo.Controls.Add(this.comboBox1);
            this.updatepanel_clo.Location = new System.Drawing.Point(0, 0);
            this.updatepanel_clo.Name = "updatepanel_clo";
            this.updatepanel_clo.Size = new System.Drawing.Size(574, 408);
            this.updatepanel_clo.TabIndex = 25;
            // 
            // deletepanel_clo
            // 
            this.deletepanel_clo.Controls.Add(this.viewStudent_clo);
            this.deletepanel_clo.Controls.Add(this.guna2GradientButton9);
            this.deletepanel_clo.Controls.Add(this.dataGridView2);
            this.deletepanel_clo.Controls.Add(this.guna2GradientButton10);
            this.deletepanel_clo.Controls.Add(this.label11);
            this.deletepanel_clo.Location = new System.Drawing.Point(3, 3);
            this.deletepanel_clo.Name = "deletepanel_clo";
            this.deletepanel_clo.Size = new System.Drawing.Size(574, 408);
            this.deletepanel_clo.TabIndex = 20;
            // 
            // guna2GradientButton9
            // 
            this.guna2GradientButton9.BorderRadius = 10;
            this.guna2GradientButton9.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton9.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton9.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton9.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton9.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton9.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton9.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton9.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton9.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton9.Location = new System.Drawing.Point(367, 331);
            this.guna2GradientButton9.Name = "guna2GradientButton9";
            this.guna2GradientButton9.Size = new System.Drawing.Size(112, 32);
            this.guna2GradientButton9.TabIndex = 23;
            this.guna2GradientButton9.Text = "Delete";
            this.guna2GradientButton9.Click += new System.EventHandler(this.guna2GradientButton9_Click_1);
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(44, 109);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(487, 194);
            this.dataGridView2.TabIndex = 22;
            // 
            // guna2GradientButton10
            // 
            this.guna2GradientButton10.BorderRadius = 10;
            this.guna2GradientButton10.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton10.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton10.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton10.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton10.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton10.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton10.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton10.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton10.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton10.Location = new System.Drawing.Point(102, 331);
            this.guna2GradientButton10.Name = "guna2GradientButton10";
            this.guna2GradientButton10.Size = new System.Drawing.Size(112, 32);
            this.guna2GradientButton10.TabIndex = 21;
            this.guna2GradientButton10.Text = "Back";
            this.guna2GradientButton10.Click += new System.EventHandler(this.guna2GradientButton10_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Franklin Gothic Demi", 14.25F);
            this.label11.ForeColor = System.Drawing.Color.Crimson;
            this.label11.Location = new System.Drawing.Point(229, 46);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 24);
            this.label11.TabIndex = 20;
            this.label11.Text = "Delete CLO";
            // 
            // guna2GradientButton8
            // 
            this.guna2GradientButton8.BorderRadius = 10;
            this.guna2GradientButton8.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton8.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton8.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton8.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton8.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton8.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton8.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton8.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton8.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton8.Location = new System.Drawing.Point(365, 321);
            this.guna2GradientButton8.Name = "guna2GradientButton8";
            this.guna2GradientButton8.Size = new System.Drawing.Size(112, 32);
            this.guna2GradientButton8.TabIndex = 19;
            this.guna2GradientButton8.Text = "Update";
            this.guna2GradientButton8.Click += new System.EventHandler(this.guna2GradientButton8_Click_1);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(42, 99);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(487, 194);
            this.dataGridView1.TabIndex = 18;
            // 
            // guna2GradientButton7
            // 
            this.guna2GradientButton7.BorderRadius = 10;
            this.guna2GradientButton7.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton7.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton7.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton7.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton7.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton7.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton7.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton7.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton7.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton7.Location = new System.Drawing.Point(100, 321);
            this.guna2GradientButton7.Name = "guna2GradientButton7";
            this.guna2GradientButton7.Size = new System.Drawing.Size(112, 32);
            this.guna2GradientButton7.TabIndex = 17;
            this.guna2GradientButton7.Text = "Back";
            this.guna2GradientButton7.Click += new System.EventHandler(this.guna2GradientButton7_Click);
            // 
            // update
            // 
            this.update.AutoSize = true;
            this.update.Font = new System.Drawing.Font("Franklin Gothic Demi", 14.25F);
            this.update.ForeColor = System.Drawing.Color.Crimson;
            this.update.Location = new System.Drawing.Point(223, 36);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(108, 24);
            this.update.TabIndex = 1;
            this.update.Text = "Update CLO";
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.LavenderBlush;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Name",
            "DateCreated",
            "DateUpdated"});
            this.comboBox1.Location = new System.Drawing.Point(408, 72);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 23;
            // 
            // viewStudent_clo
            // 
            this.viewStudent_clo.Controls.Add(this.dataGridView3);
            this.viewStudent_clo.Controls.Add(this.guna2GradientButton12);
            this.viewStudent_clo.Controls.Add(this.label12);
            this.viewStudent_clo.Location = new System.Drawing.Point(0, 0);
            this.viewStudent_clo.Name = "viewStudent_clo";
            this.viewStudent_clo.Size = new System.Drawing.Size(574, 408);
            this.viewStudent_clo.TabIndex = 24;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(44, 109);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(487, 194);
            this.dataGridView3.TabIndex = 26;
            // 
            // guna2GradientButton12
            // 
            this.guna2GradientButton12.BorderRadius = 10;
            this.guna2GradientButton12.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton12.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton12.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton12.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton12.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton12.FillColor = System.Drawing.Color.PaleVioletRed;
            this.guna2GradientButton12.FillColor2 = System.Drawing.Color.LightPink;
            this.guna2GradientButton12.Font = new System.Drawing.Font("Franklin Gothic Heavy", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2GradientButton12.ForeColor = System.Drawing.Color.Crimson;
            this.guna2GradientButton12.Location = new System.Drawing.Point(233, 331);
            this.guna2GradientButton12.Name = "guna2GradientButton12";
            this.guna2GradientButton12.Size = new System.Drawing.Size(112, 32);
            this.guna2GradientButton12.TabIndex = 25;
            this.guna2GradientButton12.Text = "Back";
            this.guna2GradientButton12.Click += new System.EventHandler(this.guna2GradientButton12_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Franklin Gothic Demi", 14.25F);
            this.label12.ForeColor = System.Drawing.Color.Crimson;
            this.label12.Location = new System.Drawing.Point(247, 46);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 24);
            this.label12.TabIndex = 24;
            this.label12.Text = "View CLOs";
            // 
            // Manage_CLOs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LavenderBlush;
            this.Controls.Add(this.deletepanel_clo);
            this.Controls.Add(this.CLOs_btn_panel);
            this.Name = "Manage_CLOs";
            this.Size = new System.Drawing.Size(580, 414);
            this.CLOs_btn_panel.ResumeLayout(false);
            this.CLOs_btn_panel.PerformLayout();
            this.addpanel_clos.ResumeLayout(false);
            this.addpanel_clos.PerformLayout();
            this.guna2GradientPanel1.ResumeLayout(false);
            this.guna2GradientPanel1.PerformLayout();
            this.updatepanel_clo.ResumeLayout(false);
            this.updatepanel_clo.PerformLayout();
            this.deletepanel_clo.ResumeLayout(false);
            this.deletepanel_clo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.viewStudent_clo.ResumeLayout(false);
            this.viewStudent_clo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel CLOs_btn_panel;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel1;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton4;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton3;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton2;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel addpanel_clos;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton6;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton5;
        private Guna.UI2.WinForms.Guna2DateTimePicker guna2DateTimePicker1;
        private Guna.UI2.WinForms.Guna2DateTimePicker guna2DateTimePicker2;
        private System.Windows.Forms.Panel updatepanel_clo;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton8;
        private System.Windows.Forms.DataGridView dataGridView1;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton7;
        private System.Windows.Forms.Label update;
        private System.Windows.Forms.Panel deletepanel_clo;
        private System.Windows.Forms.Panel viewStudent_clo;
        private System.Windows.Forms.DataGridView dataGridView3;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton12;
        private System.Windows.Forms.Label label12;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton9;
        private System.Windows.Forms.DataGridView dataGridView2;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}
