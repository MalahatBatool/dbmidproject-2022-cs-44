﻿using Guna.UI2.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectB
{
    public partial class ManageRubric : UserControl
    {
        public ManageRubric()
        {
            InitializeComponent();
            addpanel_rubrics.Visible = false;
            updatepanel_rubric.Visible = false;
            deletepanel_rubric.Visible = false;
            viewStudent_Rubric.Visible = false;
        }

        private void guna2GradientButton1_Click(object sender, EventArgs e)
        {
            //addbtn
            addpanel_rubrics.Visible = true;
            updatepanel_rubric.Visible = false;
        }

        private void guna2GradientButton2_Click(object sender, EventArgs e)
        {
            //updatebtn
            addpanel_rubrics.Visible = true;
            updatepanel_rubric.Visible = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void guna2GradientButton3_Click(object sender, EventArgs e)
        {
            //dltbtn
            addpanel_rubrics.Visible = true;
            updatepanel_rubric.Visible = true;
            deletepanel_rubric.Visible=true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView2.DataSource = dt;
        }

        private void guna2GradientButton4_Click(object sender, EventArgs e)
        {
            //viewbtn
            addpanel_rubrics.Visible = true;
            updatepanel_rubric.Visible = true;
            deletepanel_rubric.Visible = true;
            viewStudent_Rubric.Visible = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView3.DataSource = dt;
        }

        private void guna2GradientButton6_Click(object sender, EventArgs e)
        {
            //addbackbtn_rubric
            Rubric_btn_panel.Visible = true;
            addpanel_rubrics.Visible = false;
        }

        private void guna2GradientButton5_Click(object sender, EventArgs e)
        {
            //addbtn_rubric
            if (!string.IsNullOrWhiteSpace(textBox1.Text) && !string.IsNullOrWhiteSpace(textBox2.Text))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("INSERT INTO Rubric (RubricId, Details, CloId) VALUES (@RubricId, @Details, @CloId)", con);
                cmd.Parameters.AddWithValue("@RubricId", textBox1.Text);
                cmd.Parameters.AddWithValue("@Details", textBox2.Text);
                cmd.Parameters.AddWithValue("@CloId", comboBox2.SelectedValue); 
                cmd.ExecuteNonQuery();
                MessageBox.Show("Rubric successfully saved");
                con.Close(); // Close the connection
            }
            else
            {
                MessageBox.Show("Please fill in all fields");
            }
        }

        private void guna2GradientButton7_Click(object sender, EventArgs e)
        {
            //updatebackbtn_rubric
            Rubric_btn_panel.Visible = true;
            addpanel_rubrics.Visible = false;
            updatepanel_rubric.Visible = false;
        }

        private void guna2GradientButton8_Click(object sender, EventArgs e)
        {
            //updatebtn_rubric
        }

        private void guna2GradientButton10_Click(object sender, EventArgs e)
        {
            //dltbackbtn_rubric
            Rubric_btn_panel.Visible = true;
            addpanel_rubrics.Visible = false;
            updatepanel_rubric.Visible = false;
            deletepanel_rubric.Visible=false;
        }

        private void deletepanel_rubric_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2GradientButton9_Click(object sender, EventArgs e)
        {
            //dltbtn_rubric
        }

        private void guna2GradientButton12_Click(object sender, EventArgs e)
        {
            //viewbackbtn_rubrics
            Rubric_btn_panel.Visible = true;
            addpanel_rubrics.Visible = false;
            updatepanel_rubric.Visible = false;
            deletepanel_rubric.Visible = false;
            viewStudent_Rubric.Visible = false;
        }
    }
}
