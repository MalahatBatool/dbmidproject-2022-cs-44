﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectB
{
    public partial class Manage_CLOs : UserControl
    {
        public Manage_CLOs()
        {
            InitializeComponent();
            addpanel_clos.Visible = false;
            updatepanel_clo.Visible = false;
            deletepanel_clo.Visible = false;
            viewStudent_clo.Visible = false;
        }

        private void guna2GradientButton1_Click(object sender, EventArgs e)
        {
            //addbtn
            addpanel_clos.Visible = true;
            updatepanel_clo.Visible = false;

        }

        private void guna2GradientButton2_Click(object sender, EventArgs e)
        {
            //updatebtn
            addpanel_clos.Visible = true;
            updatepanel_clo.Visible = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

        }

        private void guna2GradientButton3_Click(object sender, EventArgs e)
        {
            //dltbtn
            addpanel_clos.Visible = true;
            updatepanel_clo.Visible = true;
            deletepanel_clo.Visible = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView2.DataSource = dt;

        }

        private void guna2GradientButton4_Click(object sender, EventArgs e)
        {
            //viewbtn
            addpanel_clos.Visible = true;
            updatepanel_clo.Visible = true;
            deletepanel_clo.Visible = true;
            viewStudent_clo.Visible = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView3.DataSource = dt;
        }

        private void guna2GradientButton5_Click(object sender, EventArgs e)
        {
            // Add rubric button click event handler
            if (!string.IsNullOrWhiteSpace(textBox2.Text) && !string.IsNullOrWhiteSpace(textBoxRubricId.Text))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("INSERT INTO Rubric (RubricId, Details, CloId, DateCreated, DateUpdated) VALUES (@RubricId, @Details, @CloId, @DateCreated, @DateUpdated)", con);
                cmd.Parameters.AddWithValue("@RubricId", textBoxRubricId.Text);
                cmd.Parameters.AddWithValue("@Details", textBox2.Text);
                cmd.Parameters.AddWithValue("@CloId", comboBoxCloId.SelectedValue); // Assuming you have a ComboBox to select the CLO Id
                cmd.Parameters.AddWithValue("@DateCreated", guna2DateTimePicker1.Value);
                cmd.Parameters.AddWithValue("@DateUpdated", guna2DateTimePicker2.Value);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Rubric successfully saved");
                con.Close(); // Close the connection
            }
            else
            {
                MessageBox.Show("Please fill in all fields");
            }
        }




        private void guna2GradientButton6_Click(object sender, EventArgs e)
        {
            //addclopanelbtn_back
            CLOs_btn_panel.Visible = true;
            addpanel_clos.Visible = false;
        }

        private void guna2GradientButton7_Click(object sender, EventArgs e)
        {
            //updateclopanelbtn_back
            CLOs_btn_panel.Visible = true;
            addpanel_clos.Visible = false;
            updatepanel_clo.Visible = false;
        }

        private void guna2GradientButton10_Click(object sender, EventArgs e)
        {
            //dltclopanelbtn_back
            CLOs_btn_panel.Visible = true;
            addpanel_clos.Visible = false;
            updatepanel_clo.Visible = false;
            deletepanel_clo.Visible = false;
        }

        private void guna2GradientButton12_Click(object sender, EventArgs e)
        {
            //viewclopanelbtn_back
            CLOs_btn_panel.Visible = true;
            addpanel_clos.Visible = false;
            updatepanel_clo.Visible = false;
            deletepanel_clo.Visible = false;
            viewStudent_clo.Visible = false;
        }

        private void guna2GradientButton9_Click(object sender, EventArgs e)
        {

        }

        private void guna2GradientButton9_Click_1(object sender, EventArgs e)
        {
            // dltpanelbtn
            if (dataGridView2.SelectedRows.Count > 0)
            {
                string selectedId = dataGridView2.SelectedRows[0].Cells["Id"].Value.ToString();
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("DELETE FROM Clo WHERE Id = @Id", con);
                cmd.Parameters.AddWithValue("@Id", selectedId);
                cmd.ExecuteNonQuery();
                MessageBox.Show("The selected record has been deleted");
                con.Close(); // Close the connection
            }
            else
            {
                MessageBox.Show("No row selected.");
            }
        }

        private void guna2GradientButton8_Click(object sender, EventArgs e)
        {
            
        }

        private void guna2GradientButton8_Click_1(object sender, EventArgs e)
        {
            // Update button click event handler
            if (dataGridView1.SelectedRows.Count > 0)
            {
                // Show a MessageBox with ComboBox
                DialogResult result = MessageBox.Show(comboBox1, "Select attribute to update", "Update", MessageBoxButtons.OKCancel);

                if (result == DialogResult.OK)
                {
                    // Get the selected attribute from the ComboBox
                    string selectedAttribute = comboBox1.SelectedItem?.ToString();

                    if (!string.IsNullOrEmpty(selectedAttribute))
                    {
                        // Prompt the user to enter the new value for the selected attribute
                        string newValue = Microsoft.VisualBasic.Interaction.InputBox($"Enter new {selectedAttribute}:", "Update", "");

                        if (!string.IsNullOrEmpty(newValue))
                        {
                            // Update the selected attribute in the database
                            string selectedId = dataGridView1.SelectedRows[0].Cells["Id"].Value.ToString();
                            var con = Configuration.getInstance().getConnection();
                            SqlCommand cmd = new SqlCommand($"UPDATE Clo SET {selectedAttribute.Replace(" ", "")} = @Value WHERE Id = @Id", con);
                            cmd.Parameters.AddWithValue("@Value", newValue);
                            cmd.Parameters.AddWithValue("@Id", selectedId);
                            cmd.ExecuteNonQuery();

                            MessageBox.Show($"{selectedAttribute} updated successfully");
                        }
                        else
                        {
                            MessageBox.Show("Please enter a new value.");
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select a row to update.");
            }
        }
    }
}