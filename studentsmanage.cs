﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectB
{
    public partial class studentsmanage : UserControl
    {
        public studentsmanage()
        {
            InitializeComponent();
            addpanel.Visible = false;
            updatepanel.Visible = false;
            deletepanel.Visible = false;
            viewStudent.Visible = false;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2GradientButton1_Click(object sender, EventArgs e)
        {
            //addbtn
            addpanel.Visible = true;
            updatepanel.Visible = false;
        }

        private void guna2GradientButton2_Click(object sender, EventArgs e)
        {
            //updatebtn
            addpanel.Visible = true;
            updatepanel.Visible = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void guna2GradientButton3_Click(object sender, EventArgs e)
        {
            //dltbtn
            addpanel.Visible = true;
            updatepanel.Visible = true;
            deletepanel.Visible = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView2.DataSource = dt;
        }

        private void guna2GradientButton4_Click(object sender, EventArgs e)
        {
            //viewbtn
            addpanel.Visible = true;
            updatepanel.Visible = true;
            deletepanel.Visible = true;
            viewStudent.Visible = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView3.DataSource = dt;
        }

        private void guna2GradientButton6_Click(object sender, EventArgs e)
        {
            //addpanelbackbtn
            btnpanel.Visible = true;
            addpanel.Visible = false;
        }

        private void guna2GradientButton5_Click(object sender, EventArgs e)
        {
            //addpanelbtn
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" &&
                textBox5.Text != "" && textBox6.Text != "" && textBox7.Text != "")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("INSERT INTO Student (FirstName, LastName, Contact, Email, RegistrationNumber, Status) VALUES (@FirstName, @LastName, @Contact, @Email, @RegistrationNumber, @Status)", con);
                cmd.Parameters.AddWithValue("@FirstName", textBox2.Text);
                cmd.Parameters.AddWithValue("@LastName", textBox3.Text);
                cmd.Parameters.AddWithValue("@Contact", textBox4.Text);
                cmd.Parameters.AddWithValue("@Email", textBox5.Text);
                cmd.Parameters.AddWithValue("@RegistrationNumber", textBox6.Text);
                cmd.Parameters.AddWithValue("@Status", textBox7.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
            }
            else
            {
                MessageBox.Show("Please enter values for all fields");
            }
        }

        private void guna2GradientButton7_Click(object sender, EventArgs e)
        {
            //updatebackpanel
            btnpanel.Visible = true;
            addpanel.Visible = false;
            updatepanel.Visible = false;
        }

        private void guna2GradientButton8_Click(object sender, EventArgs e)
        {
            //upadtepanelbtn
                // Update button click event handler
                if (dataGridView1.SelectedRows.Count > 0)
                {
                    // Show a MessageBox with ComboBox
                    DialogResult result = MessageBox.Show(comboBox1, "Select attribute to update", "Update", MessageBoxButtons.OKCancel);

                    if (result == DialogResult.OK)
                    {
                        // Get the selected attribute from the ComboBox
                        string selectedAttribute = comboBox1.SelectedItem?.ToString();

                        if (!string.IsNullOrEmpty(selectedAttribute))
                        {
                            // Prompt the user to enter the new value for the selected attribute
                            string newValue = Microsoft.VisualBasic.Interaction.InputBox($"Enter new {selectedAttribute}:", "Update", "");

                            if (!string.IsNullOrEmpty(newValue))
                            {
                                // Update the selected attribute in the database
                                string selectedId = dataGridView1.SelectedRows[0].Cells["Id"].Value.ToString();
                                var con = Configuration.getInstance().getConnection();
                                SqlCommand cmd = new SqlCommand($"UPDATE Student SET {selectedAttribute.Replace(" ", "")} = @Value WHERE Id = @Id", con);
                                cmd.Parameters.AddWithValue("@Value", newValue);
                                cmd.Parameters.AddWithValue("@Id", selectedId);
                                cmd.ExecuteNonQuery();

                                MessageBox.Show($"{selectedAttribute} updated successfully");
                            }
                            else
                            {
                                MessageBox.Show("Please enter a new value.");
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Please select a row to update.");
                }

        }

        private void guna2GradientButton9_Click(object sender, EventArgs e)
        {
            // dltpanelbtn
            if (dataGridView2.SelectedRows.Count > 0)
            {
                string selectedId = dataGridView2.SelectedRows[0].Cells["Id"].Value.ToString();
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("DELETE FROM Student WHERE Id = @Id", con);
                cmd.Parameters.AddWithValue("@Id", selectedId);
                cmd.ExecuteNonQuery();
                MessageBox.Show("The selected record has been deleted");
                con.Close(); // Close the connection
            }
            else
            {
                MessageBox.Show("No row selected.");
            }
        }


        private void guna2GradientButton10_Click(object sender, EventArgs e)
        {
            //dltbackbtn
            btnpanel.Visible = true;
            addpanel.Visible = false;
            updatepanel.Visible = false;
            deletepanel.Visible = false;
        }

        private void guna2GradientButton12_Click(object sender, EventArgs e)
        {
            //BACKFROMVIEW
            btnpanel.Visible = true;
            addpanel.Visible = false;
            updatepanel.Visible = false;
            deletepanel.Visible = false;
            viewStudent.Visible = false;
        }

        private void viewStudent_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
