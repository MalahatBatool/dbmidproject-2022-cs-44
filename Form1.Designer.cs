﻿namespace ProjectB
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.guna2ControlBox2 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox1 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2Button1 = new Guna.UI2.WinForms.Guna2Button();
            this.SubMenu = new System.Windows.Forms.Panel();
            this.guna2Button8 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button7 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button6 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button5 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button4 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button3 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button2 = new Guna.UI2.WinForms.Guna2Button();
            this.manageRubric1 = new ProjectB.ManageRubric();
            this.manage_CLOs1 = new ProjectB.Manage_CLOs();
            this.home11 = new ProjectB.home1();
            this.studentsmanage1 = new ProjectB.studentsmanage();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SubMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LavenderBlush;
            this.panel1.Controls.Add(this.manageRubric1);
            this.panel1.Controls.Add(this.manage_CLOs1);
            this.panel1.Controls.Add(this.home11);
            this.panel1.Controls.Add(this.studentsmanage1);
            this.panel1.Location = new System.Drawing.Point(223, 37);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(618, 468);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.guna2ControlBox2);
            this.panel2.Controls.Add(this.guna2ControlBox1);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(853, 31);
            this.panel2.TabIndex = 1;
            // 
            // guna2ControlBox2
            // 
            this.guna2ControlBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.guna2ControlBox2.FillColor = System.Drawing.Color.LightPink;
            this.guna2ControlBox2.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox2.Location = new System.Drawing.Point(788, 3);
            this.guna2ControlBox2.Name = "guna2ControlBox2";
            this.guna2ControlBox2.Size = new System.Drawing.Size(28, 26);
            this.guna2ControlBox2.TabIndex = 1;
            // 
            // guna2ControlBox1
            // 
            this.guna2ControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox1.FillColor = System.Drawing.Color.LightPink;
            this.guna2ControlBox1.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox1.Location = new System.Drawing.Point(822, 2);
            this.guna2ControlBox1.Name = "guna2ControlBox1";
            this.guna2ControlBox1.Size = new System.Drawing.Size(28, 26);
            this.guna2ControlBox1.TabIndex = 0;
            // 
            // guna2Button1
            // 
            this.guna2Button1.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button1.FillColor = System.Drawing.Color.LightPink;
            this.guna2Button1.Font = new System.Drawing.Font("Franklin Gothic Demi", 14.25F);
            this.guna2Button1.ForeColor = System.Drawing.Color.Crimson;
            this.guna2Button1.Location = new System.Drawing.Point(12, 91);
            this.guna2Button1.Name = "guna2Button1";
            this.guna2Button1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.guna2Button1.Size = new System.Drawing.Size(180, 45);
            this.guna2Button1.TabIndex = 2;
            this.guna2Button1.Text = "Menu";
            this.guna2Button1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button1.Click += new System.EventHandler(this.guna2Button1_Click);
            // 
            // SubMenu
            // 
            this.SubMenu.BackColor = System.Drawing.Color.LavenderBlush;
            this.SubMenu.Controls.Add(this.guna2Button8);
            this.SubMenu.Controls.Add(this.guna2Button7);
            this.SubMenu.Controls.Add(this.guna2Button6);
            this.SubMenu.Controls.Add(this.guna2Button5);
            this.SubMenu.Controls.Add(this.guna2Button4);
            this.SubMenu.Controls.Add(this.guna2Button3);
            this.SubMenu.Controls.Add(this.guna2Button2);
            this.SubMenu.Location = new System.Drawing.Point(9, 142);
            this.SubMenu.Name = "SubMenu";
            this.SubMenu.Size = new System.Drawing.Size(208, 300);
            this.SubMenu.TabIndex = 3;
            // 
            // guna2Button8
            // 
            this.guna2Button8.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button8.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button8.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button8.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button8.FillColor = System.Drawing.Color.LavenderBlush;
            this.guna2Button8.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2Button8.ForeColor = System.Drawing.Color.Crimson;
            this.guna2Button8.Location = new System.Drawing.Point(1, 248);
            this.guna2Button8.Name = "guna2Button8";
            this.guna2Button8.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.guna2Button8.Size = new System.Drawing.Size(208, 40);
            this.guna2Button8.TabIndex = 6;
            this.guna2Button8.Text = "Mark Evaluations";
            this.guna2Button8.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // guna2Button7
            // 
            this.guna2Button7.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button7.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button7.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button7.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button7.FillColor = System.Drawing.Color.LavenderBlush;
            this.guna2Button7.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2Button7.ForeColor = System.Drawing.Color.Crimson;
            this.guna2Button7.Location = new System.Drawing.Point(1, 207);
            this.guna2Button7.Name = "guna2Button7";
            this.guna2Button7.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.guna2Button7.Size = new System.Drawing.Size(208, 40);
            this.guna2Button7.TabIndex = 5;
            this.guna2Button7.Text = "Manage Rubric Levels";
            this.guna2Button7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // guna2Button6
            // 
            this.guna2Button6.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button6.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button6.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button6.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button6.FillColor = System.Drawing.Color.LavenderBlush;
            this.guna2Button6.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2Button6.ForeColor = System.Drawing.Color.Crimson;
            this.guna2Button6.Location = new System.Drawing.Point(1, 166);
            this.guna2Button6.Name = "guna2Button6";
            this.guna2Button6.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.guna2Button6.Size = new System.Drawing.Size(208, 40);
            this.guna2Button6.TabIndex = 4;
            this.guna2Button6.Text = "Manage Assessments";
            this.guna2Button6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // guna2Button5
            // 
            this.guna2Button5.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button5.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button5.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button5.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button5.FillColor = System.Drawing.Color.LavenderBlush;
            this.guna2Button5.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2Button5.ForeColor = System.Drawing.Color.Crimson;
            this.guna2Button5.Location = new System.Drawing.Point(1, 125);
            this.guna2Button5.Name = "guna2Button5";
            this.guna2Button5.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.guna2Button5.Size = new System.Drawing.Size(208, 40);
            this.guna2Button5.TabIndex = 3;
            this.guna2Button5.Text = "Manage Rubrics";
            this.guna2Button5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button5.Click += new System.EventHandler(this.guna2Button5_Click);
            // 
            // guna2Button4
            // 
            this.guna2Button4.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button4.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button4.FillColor = System.Drawing.Color.LavenderBlush;
            this.guna2Button4.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2Button4.ForeColor = System.Drawing.Color.Crimson;
            this.guna2Button4.Location = new System.Drawing.Point(1, 84);
            this.guna2Button4.Name = "guna2Button4";
            this.guna2Button4.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.guna2Button4.Size = new System.Drawing.Size(208, 40);
            this.guna2Button4.TabIndex = 2;
            this.guna2Button4.Text = "Manage CLOs";
            this.guna2Button4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button4.Click += new System.EventHandler(this.guna2Button4_Click);
            // 
            // guna2Button3
            // 
            this.guna2Button3.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button3.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button3.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button3.FillColor = System.Drawing.Color.LavenderBlush;
            this.guna2Button3.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2Button3.ForeColor = System.Drawing.Color.Crimson;
            this.guna2Button3.Location = new System.Drawing.Point(1, 43);
            this.guna2Button3.Name = "guna2Button3";
            this.guna2Button3.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.guna2Button3.Size = new System.Drawing.Size(208, 40);
            this.guna2Button3.TabIndex = 1;
            this.guna2Button3.Text = "Manage Students";
            this.guna2Button3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button3.Click += new System.EventHandler(this.guna2Button3_Click);
            // 
            // guna2Button2
            // 
            this.guna2Button2.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button2.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button2.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button2.FillColor = System.Drawing.Color.LavenderBlush;
            this.guna2Button2.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Italic);
            this.guna2Button2.ForeColor = System.Drawing.Color.Crimson;
            this.guna2Button2.Location = new System.Drawing.Point(1, 2);
            this.guna2Button2.Name = "guna2Button2";
            this.guna2Button2.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.guna2Button2.Size = new System.Drawing.Size(208, 40);
            this.guna2Button2.TabIndex = 0;
            this.guna2Button2.Text = "Home";
            this.guna2Button2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button2.Click += new System.EventHandler(this.guna2Button2_Click);
            // 
            // manageRubric1
            // 
            this.manageRubric1.BackColor = System.Drawing.Color.LavenderBlush;
            this.manageRubric1.Location = new System.Drawing.Point(20, 26);
            this.manageRubric1.Name = "manageRubric1";
            this.manageRubric1.Size = new System.Drawing.Size(580, 414);
            this.manageRubric1.TabIndex = 4;
            // 
            // manage_CLOs1
            // 
            this.manage_CLOs1.BackColor = System.Drawing.Color.LavenderBlush;
            this.manage_CLOs1.Location = new System.Drawing.Point(20, 26);
            this.manage_CLOs1.Name = "manage_CLOs1";
            this.manage_CLOs1.Size = new System.Drawing.Size(580, 414);
            this.manage_CLOs1.TabIndex = 3;
            // 
            // home11
            // 
            this.home11.BackColor = System.Drawing.Color.LavenderBlush;
            this.home11.Location = new System.Drawing.Point(20, 26);
            this.home11.Name = "home11";
            this.home11.Size = new System.Drawing.Size(580, 414);
            this.home11.TabIndex = 2;
            // 
            // studentsmanage1
            // 
            this.studentsmanage1.BackColor = System.Drawing.Color.LavenderBlush;
            this.studentsmanage1.Location = new System.Drawing.Point(20, 26);
            this.studentsmanage1.Name = "studentsmanage1";
            this.studentsmanage1.Size = new System.Drawing.Size(580, 414);
            this.studentsmanage1.TabIndex = 1;
            this.studentsmanage1.Load += new System.EventHandler(this.studentsmanage1_Load);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightPink;
            this.ClientSize = new System.Drawing.Size(853, 517);
            this.Controls.Add(this.SubMenu);
            this.Controls.Add(this.guna2Button1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.SubMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox2;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox1;
        private Guna.UI2.WinForms.Guna2Button guna2Button1;
        private System.Windows.Forms.Panel SubMenu;
        private Guna.UI2.WinForms.Guna2Button guna2Button5;
        private Guna.UI2.WinForms.Guna2Button guna2Button4;
        private Guna.UI2.WinForms.Guna2Button guna2Button3;
        private Guna.UI2.WinForms.Guna2Button guna2Button2;
        private Guna.UI2.WinForms.Guna2Button guna2Button8;
        private Guna.UI2.WinForms.Guna2Button guna2Button7;
        private Guna.UI2.WinForms.Guna2Button guna2Button6;
        private studentsmanage studentsmanage1;
        private home1 home1;
        private home1 home11;
        private Manage_CLOs manage_CLOs1;
        private ManageRubric manageRubric1;
    }
}

